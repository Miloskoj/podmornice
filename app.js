"use strict";

(function () {
    makeTerrain(10); // var teren =
    var brojGadjanja=0;
    var brojPogodaka=0;
    var preciznost=0;
    var s1 = addSmallFleet(10);
    var s2 = addMediumFleet(10);
    var s3 = addBiggerFleet(10);
    var s4 = addBiggestFleet(10);
    var gadjanaPolja=[];

    var matrix = getMatrix(10);
    addCallbackToField(matrix, function handleClick(event) {     // closure
        brojGadjanja++;
        var arr = event.target.id.split('_');

        var i = parseInt(arr[1], 10);
        var j = parseInt(arr[2], 10);
        console.log("Koordinata X " + i);
        console.log("Koordinata Y " + j);
        var k;
        var temp=0;
        temp=0;
        for (k=0;k<s1.length;k++)
            {
            if ((i == s1[k].x) && (j == s1[k].y))
                    {
                        //matrix[i][j].css("background-color", "black");
                        matrix[i][j].css("background-image", "url(pogodak.jpg)");
                        matrix[i][j].css("background-size", "100% 100%");
                    brojPogodaka++;
                    temp++;
                    }

            }
        for (k=0;k<s2.length;k++) {
            if ((s2[k].x1 === j && s2[k].y1 === i) || (s2[k].x2 === j && s2[k].y2 === i)) {
                //matrix[i][j].css("background-color", "black");
                matrix[i][j].css("background-image", "url(pogodak.jpg)");
                matrix[i][j].css("background-size", "100% 100%");
                brojPogodaka++;
                temp++;
            }
        }
        for (k=0;k<s3.length;k++) {
            if ((s3[k].x1 === j && s3[k].y1 === i) || (s3[k].x2 === j && s3[k].y2 === i) || (s3[k].x3 === j && s3[k].y3 === i) ) {
                //matrix[i][j].css("background-color", "black");
                matrix[i][j].css("background-image", "url(pogodak.jpg)");
                matrix[i][j].css("background-size", "100% 100%");
                brojPogodaka++;
                temp++;
            }
        }
        for (k=0;k<s4.length;k++) {
            if ((s4[k].x1 === j && s4[k].y1 === i) || (s4[k].x2 === j && s4[k].y2 === i) || (s4[k].x3 === j && s4[k].y3 === i)|| (s4[k].x4 === j && s4[k].y4 === i) ) {
                //matrix[i][j].css("background-color", "black");
                matrix[i][j].css("background-image", "url(pogodak.jpg)");
                matrix[i][j].css("background-size", "100% 100%");
                brojPogodaka++;
                temp++;
            }
        }
        gadjanaPolja.push(i,j);
        preciznost=brojPogodaka/brojGadjanja*100;
        console.log(gadjanaPolja);
        console.log("Broj gadjanja " + brojGadjanja + " , Broj pogodaka " + brojPogodaka + " , preciznost " + preciznost + " %");

        if(temp==0)
            {

            matrix[i][j].css("background-image", "url(promasaj.jpg)");
            matrix[i][j].css("background-size", "100% 100%");
            }
        if(brojPogodaka === 20) alert("Bravo admirale, potopili ste sve brodove !  :)");

    });

    $('#showFleet').click(function () {  // Prikazuje polozaj brodova na ekranu
        var i, j, k;
        for (i = 0; i < matrix.length; i++) {  // Male
            for (j = 0; j < matrix.length; j++) {
                for (k = 0; k < s1.length; k++) {
                    if (s1[k].x === i && s1[k].y === j) {
                        matrix[i][j].css( "background-color", "red" );
                       // ("background-image", "url(/myimage.jpg)");
                        matrix[i][j].css("background-image", "url(ship.png)");
                        matrix[i][j].css("background-size", "100% 100%");

                        //background-repeat: repeat-x;
                        //$('#divID').css("background-image", "url(/myimage.jpg)");
                    }
                }
            }
        }
        for (i = 0; i < matrix.length; i++) {  // Srednje
            for (j = 0; j < matrix.length; j++) {
                for (k = 0; k < s2.length; k++) {
                    if ((s2[k].x1 === j && s2[k].y1 === i)||(s2[k].x2 === j && s2[k].y2===i)) {
                        matrix[i][j].css( "background-color", "red" );
                        matrix[i][j].css("background-image", "url(ship.png)");
                        matrix[i][j].css("background-size", "100% 100%");

                    }
                }
            }
        }
        for (i = 0; i < matrix.length; i++)   // Vece
            for (j = 0; j < matrix.length; j++)
                for (k = 0; k < s3.length; k++)
                    if ((s3[k].x1 === j && s3[k].y1 === i) || (s3[k].x2 === j && s3[k].y2 === i) || (s3[k].x3 === j && s3[k].y3 === i) ) {
                        matrix[i][j].css("background-color", "red" );
                        matrix[i][j].css("background-image", "url(ship.png)");
                        matrix[i][j].css("background-size", "100% 100%");

                    }

        for (i = 0; i < matrix.length; i++)   // Vece
            for (j = 0; j < matrix.length; j++)
                for (k = 0; k < s4.length; k++)
                    if ((s4[k].x1 === j && s4[k].y1 === i) || (s4[k].x2 === j && s4[k].y2 === i) || (s4[k].x3 === j && s4[k].y3 === i)|| (s4[k].x4 === j && s4[k].y4 === i) ) {
                        matrix[i][j].css("background-color", "red" );
                        matrix[i][j].css("background-image", "url(ship.png)");
                        matrix[i][j].css("background-size", "100% 100%");

                    }


    }); // Prikazuje polozaj brodova na ekranu


    $('#hideFleet').click(function () {  // Sakriva polozaj brodova
        var i, j;
        for (i = 0; i < matrix.length; i++)
            for (j = 0; j < matrix.length; j++)
            {
                    matrix[i][j].css("background-color", '#B0E0E6');
                    matrix[i][j].css("background-image", "none");
            }

    }); // Sakriva polozaj brodova na ekranu

})();

function addCallbackToField(matrix, cb) {
    var i, j;
    for (i = 0; i < matrix.length; i++) {
        for (j = 0; j < matrix.length; j++) {
            matrix[i][j].click(cb);
        }
    }
}

function makeTerrain(dim) {
    var i, j;
    var container = $("#container");

    var common = 'style="width: 49px; height: 49px; position: absolute; border: 1px solid black; ';
    var padding = ''; // 'padding: 10px 10px 10px 10px; ';

    for(i = 0; i < dim; i++) {
        for(j = 0; j < dim; j++) {
            var id = 'id="id_' + i + '_' + j + '"';
            var top = 'top: ' + i * 50 + 'px; ';
            var left = 'left: ' + j * 50 + 'px; ';

            var style = common + top + left + padding + 'background-color: #B0E0E6"';

            container.append("<div " + id + " " + style + "></div>");
        }
    }
}
function addSmallFleet(dim) {
    var s1 = [];
    var i;
    for (i = 0; i < 4; i++) {
        s1.push({
            x: getRandomInt(0, 10),
            y: getRandomInt(0, 10)
        });
    }

    return s1;
}

function addMediumFleet(dim) {
    var s2 = [];
    var i,j,k,y;
    for (i = 0; i < 3; i++) {
        j=getRandomInt(0, 10);
        y=getRandomInt(0, 10);
        if(j<9)
            k=j+1;
        else
            k=8;
        s2.push({
            x1: j,
            y1: y,
            x2: k,
            y2: y
        });
    }

    return s2;

}

function addBiggerFleet(dim) {  //  Flota od dva broda sa tri polja
    var s3 = [];
    var i,x,y,rotacija;

    for (i = 0; i < 2; i++) {
        rotacija=getRandomInt(0,2);
        if (rotacija>1)  // Horizontala
            {
                x=getRandomInt(0, 7);
                y=getRandomInt(0, 10);

                s3.push({
                x1: x,
                y1: y,
                x2: x+1,
                y2: y,
                x3: x + 2,
                y3: y
                });
            }
        else  // Vertikala
            {
                x=getRandomInt(0, 10);
                y=getRandomInt(0, 7);

                s3.push({
                    x1: x,
                    y1: y,
                    x2: x,
                    y2: y+1,
                    x3: x,
                    y3: y+2
                });
            }
        }

    return s3;

}

function addBiggestFleet(dim) {  // Brod od 4 polja
    var s4 = [];
    var i,x,k,y,rotacija;
    rotacija=getRandomInt(0,2);


    if (rotacija>1)  // Horizontala
       {
           x=getRandomInt(0, 6);
           y=getRandomInt(0, 10);

           s3.push({
               x1: x,
               y1: y,
               x2: x+1,
               y2: y,
               x3: x+2,
               y3: y,
               x4: x+3,
               y4: y
           });
       }
    else {  // Vertikala
        x=getRandomInt(0, 10);
        y=getRandomInt(0, 6);

        s4.push({
            x1: x,
            y1: y,
            x2: x,
            y2: y + 1,
            x3: x,
            y3: y + 2,
            x4: x,
            y4: y + 3
        });
    }
    return s4;

}


function getMatrix(dim) {
    var i, j;
    var matrix = [];
    for (i = 0; i < dim; i++) {
        var row = [];
        for (j = 0; j < dim; j++) {
            var elem = $('#id_' + i + '_' + j);
            row.push(elem);
        }

        matrix.push(row);
    }

    return matrix;
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}


